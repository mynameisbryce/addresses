jQuery(function($){
	$(document).ready(function(){

		SubNavigation = function(){
			$('header#masthead span.resources').hover(function(){
				MenuOpenCloseErgoTimer(0, function(node){
					console.log('on');
					$('ul.resources').addClass('activate');
					$('span.resources').addClass('activate');
					$('ul.sub-nav.solutions').removeClass('activate');
					$('span.solutions').removeClass('activate');

				});
				
			}, function(){
				MenuOpenCloseErgoTimer(200, function(){
					console.log('off');
					$('ul.resources').removeClass('activate');
					$('span.resources').removeClass('activate');
				});
			});

			$('header#masthead span.solutions').hover(function(){

				MenuOpenCloseErgoTimer(0, function(node){
					console.log('on');
					$('ul.sub-nav.solutions').addClass('activate');
					$('span.solutions').addClass('activate');
					$('ul.sub-nav.resources').removeClass('activate');
					$('span.resources').removeClass('activate');
				});
				
			}, function(){
				MenuOpenCloseErgoTimer(200, function(){
					console.log('off');
					$('ul.sub-nav.solutions').removeClass('activate');
					$('span.solutions').removeClass('activate');
				});
			});

			$('ul.solutions').hover(function(){

				MenuOpenCloseErgoTimer(0, function(){
					console.log('on');
					$('ul.sub-nav.solutions').addClass('activate');
					$('span.solutions').addClass('activate');
					$('ul.sub-nav.resources').removeClass('activate');
					$('span.resources').removeClass('activate');

				});
				
			}, function(){
				MenuOpenCloseErgoTimer(200, function(){
					console.log('off');
					$('ul.sub-nav.solutions').removeClass('activate');
					$('span.solutions').removeClass('activate');
				});
			});

			$('ul.resources').hover(function(){

				MenuOpenCloseErgoTimer(0, function(){
					console.log('on');
					$('ul.resources').addClass('activate');
					$('span.resources').addClass('activate');
					$('ul.sub-nav.solutions').removeClass('activate');
					$('span.solutions').removeClass('activate');

				});
				
			}, function(){
				MenuOpenCloseErgoTimer(200, function(){
					console.log('off');
					$('ul.resources').removeClass('activate');
					$('span.resources').removeClass('activate');
				});
			});

			//Mobile Menu Behavior
			var button = $('#mobile-menu');
			button.click(function(){
			    var dataid=button.attr('data-id');
			  
				if(dataid==1){
					$('#mobile-menu').attr("data-id","0");
					$('#mobile-menu h6').text('Close');
					$('#mobile-menu').addClass('activate');
					$('ul.top').addClass('activate');
					$('header#masthead form').addClass('activate');
				}else{
					$('#mobile-menu').attr("data-id","1");
					$('#mobile-menu h6').text('Menu');
					$('#mobile-menu').removeClass('activate');
					$('ul.top').removeClass('activate');
					$('header#masthead form').removeClass('activate');
				}
			});
		}

		MenuOpenCloseErgoTimer = function(dDelay, fActionFunction, node){
			    if (typeof this.delayTimer == "number") {
			        clearTimeout (this.delayTimer);
			        this.delayTimer = '';
			    }
			    if (node)
			        this.delayTimer     = setTimeout (function() { fActionFunction (node); }, dDelay);
			    else
			        this.delayTimer     = setTimeout (function() { fActionFunction (); }, dDelay);
		}

		SubNavigation();


	});
});
